const path = require('path')

const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const pkg = require('./package')
const dist = path.join(__dirname, 'dist')
module.exports = {
	target: 'web',
	mode: 'none',
	entry: path.join(__dirname, 'index.js'),
	output: {
		path: dist,
		filename: `${pkg.name}.js`
	},
	devServer: {static: dist},
	devtool: "source-map",
	resolve: {
		alias: {svelte: path.resolve('node_modules', 'svelte')},
		extensions: ['.mjs', '.js', '.svelte'],
		mainFields: ['svelte', 'browser', 'module', 'main']
	},
	module: {
		rules: [{
			test: /\.(html|svelte)$/,
			use: {loader: 'svelte-loader'}
		}]
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: (
				pkg.name? pkg.name: __dirname.match(/[^\\/]+$/m)[0]
			).replace(/-/g, ' ').replace(/\b\w/g, c=> c.toUpperCase()),
			favicon: 'blank.png',
			meta: {viewport: 'width=device-width, shrink-to-fit=yes'}
		}),
		new webpack.DefinePlugin({
			'process.env.SERVER': JSON.stringify(process.env.SERVER),
			'process.env.RELAY_URL': JSON.stringify(process.env.RELAY_URL),
		})
	]
}
